source ~/.zsh_aliases

# Set up the prompt

autoload -Uz colors
colors

. /etc/bash_completion.d/git-prompt

precmd () {
    SEP="%B$fg[magenta] • %b$fg[green]"
    # SEP="%B$fg[magenta] • %b$fg[cyan]"
    additions=""

    gitps1="$(__git_ps1)"
    if [ -n "$gitps1" ]; then
        additions+=$SEP${gitps1:2:${#gitps1}-3}
    fi

    if [ -n "$CONDA_DEFAULT_ENV" ]; then
        additions+=$SEP$CONDA_DEFAULT_ENV
    fi

    if [ -n "$VIRTUAL_ENV" ]; then
        additions+=$SEP${VIRTUAL_ENV##*/}
    fi

    if [ -n "$NIX_STORE" ]; then
        additions+=$SEP"nix-shell"
    fi

    print -P "%B$fg[magenta]┌─── $fg[green]%~$additions"
    # print -P "%B$fg[magenta]┌─── $fg[cyan]%~$additions"
}

PROMPT="%{%B$fg[magenta]%}└ %{$reset_color%b%}"
# PROMPT="%{%B$fg[magenta]%}└ %{$reset_color%b%}"
# RPROMPT="$(date +%R)"

source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
# source /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh
typeset -A ZSH_HIGHLIGHT_STYLES
ZSH_HIGHLIGHT_STYLES[alias]='fg=cyan'
ZSH_HIGHLIGHT_STYLES[builtin]='fg=cyan'
ZSH_HIGHLIGHT_STYLES[command]='fg=cyan'
ZSH_HIGHLIGHT_STYLES[commandseparator]='fg=magenta,bold'
ZSH_HIGHLIGHT_STYLES[double-hyphen-option]='fg=magenta'
ZSH_HIGHLIGHT_STYLES[function]='fg=cyan'
ZSH_HIGHLIGHT_STYLES[globbing]='fg=yellow,bold'
ZSH_HIGHLIGHT_STYLES[precommand]='fg=cyan,underline'
ZSH_HIGHLIGHT_STYLES[redirection]='fg=magenta,bold'
ZSH_HIGHLIGHT_STYLES[single-hyphen-option]='fg=magenta'
ZSH_HIGHLIGHT_STYLES[unknown-token]='none'

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    #alias fgrep='fgrep --color=auto'
    #alias egrep='egrep --color=auto'
fi

setopt histignorealldups sharehistory autocd

# Use emacs keybindings even if our EDITOR is set to vi
# bindkey -e

# Keep 1000 lines of history within the shell and save it to ~/.zsh_history:
HISTSIZE=10000
SAVEHIST=10000
HISTFILE=~/.zsh_history

# Use modern completion system
autoload -Uz compinit
compinit

zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _correct _approximate
zstyle ':completion:*' format "%{$fg_bold[cyan]%}→ %d"
zstyle ':completion:*' group-name ''
zstyle ':completion:*' menu select=2
eval "$(dircolors -b)"
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=* l:|=*'
zstyle ':completion:*' menu select=long
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose false

zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'
zstyle ':completion:*:kill:*' command 'ps -u $USER -o pid,%cpu,tty,cputime,cmd'

# zle_highlight=(none)


export EDITOR="nvim"
export VISUAL="nvim"

if [ -f ~/Desktop/nnn/misc/quitcd/quitcd.bash_sh_zsh ]; then
    source ~/Desktop/nnn/misc/quitcd/quitcd.bash_sh_zsh
fi

# export FZF_DEFAULT_OPTS='--color bg+:5,border:15,spinner:14,hl:6,prompt:13,info:12,pointer:14,marker:11,fg+:15,hl+:6,gutter:-1'
export FZF_DEFAULT_OPTS="
	--color=fg:#908caa,bg:#191724,hl:#ebbcba
	--color=fg+:#e0def4,bg+:#26233a,hl+:#ebbcba
	--color=border:#403d52,header:#31748f,gutter:#191724
	--color=spinner:#f6c177,info:#9ccfd8
	--color=pointer:#c4a7e7,marker:#eb6f92,prompt:#908caa"

export NNN_COLORS="5362"
BLK="0B" CHR="0B" DIR="06" EXE="06" REG="05" HARDLINK="06" SYMLINK="06" MISSING="08" ORPHAN="09" FIFO="06" SOCK="0B" OTHER="06"
export NNN_FCOLORS="$BLK$CHR$DIR$EXE$REG$HARDLINK$SYMLINK$MISSING$ORPHAN$FIFO$SOCK$OTHER"
export NNN_TRASH=1
export NNN_BMS="p:$HOME/Documents/Notes/pdf;t:$HOME/Documents/Notes/tex;"

export PATH=$PATH:~/.cargo/bin
export PATH=$PATH:~/go/bin
export PATH=$PATH:/home/linuxbrew/.linuxbrew/bin
export PATH=$PATH:~/.local/share/coursier/bin

export PATH=~/.nix-profile/bin:$PATH

export PATH=$PATH:~/Desktop/vcpkg

export GRIM_DEFAULT_DIR=~/Pictures/Screenshots

export BEMENU_OPTS='-c -W 0.5 -l 16 -p "" --fixed-height --fn "BlexMono Nerd Font Mono 16" bemenu-run -c -W 0.5 -l 16 -p "" --fixed-height --fn "BlexMono Nerd Font Mono 16" --nb "#191724" --fb "#191724" --ab "#191724" --hb "#26233a" --nf "#e0def4" --ff "#e0def4" --af "#e0def4" --hf "#c4a7e7" -B 2 --bdr "#6e6a86"'

# export GDK_DPI_SCALE=1.5

export XDG_CURRENT_DESKTOP=GNOME

# export QT_STYLE_OVERRIDE=adwaita
# export QT_QPA_PLATFORMTHEME=gnome

# WARNING
# WARNING
export PATH=$PATH:~/Desktop/emsdk
export PATH=$PATH:~/Desktop/emsdk/upstream/emscripten
# WARNING
# WARNING
