# fzf keybindings
if [ -f /usr/share/doc/fzf/examples/key-bindings.zsh ]; then
    source /usr/share/doc/fzf/examples/key-bindings.zsh
fi

alias update='sudo apt update && sudo apt upgrade && sudo apt autoremove --purge && sudo apt autoclean && tput setaf 6 && figlet -ct -f kmono Complete'
# cargo install-update -a

alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'
alias cyb='chafa -s 80x80 -c 16 ~/Pictures/cyb/$(ls ~/Pictures/cyb | shuf -n 1)'
alias bigst='st -f "BlexMono Nerd Font Mono:pixelsize=32:antialias=true:autohint=true"'
alias keyspd='xset r rate 300 50'
alias vertmon='xrandr --output DP-3 --rotate left --above eDP-1 && feh --bg-fill --no-fehbg ~/Pictures/Wallpapers/juliabg.png ~/Pictures/Wallpapers/juliavertbg.png'
alias slock='slock -m "$(cat ~/Desktop/slock/message.txt)"'
alias nosleep='xset s off -dpms'
# alias s=startx
alias s=hyprland
alias v='nvim'
alias vi='nvim'
alias vim='nvim'
alias la='ls -A'
alias clang-format="clang-format -style=file:/home/kalankaboom/.config/.clang-format"
alias pymol='python3 -m pymol -x'
alias bc='bc ~/.bcrc -ql'
alias metro='~/.custom/./metro.sh'
alias :q='exit'
alias plot='ipython3 --no-confirm-exit --no-banner -i ~/.custom/plot.py'
alias python='python3 -q'
alias ipython='ipython3 --no-confirm-exit --no-banner'
alias bastet='~/Desktop/bastet/./bastet'
alias cbonsai='cbonsai -lit .5'
alias nyancat="~/Desktop/nyancat/src/./nyancat"
alias ncm="ncmpcpp -q"
alias zat='zathura --fork'
alias cyb='chafa --scale max --symbols braille -c 16 ~/Pictures/cyb/$(ls ~/Pictures/cyb | shuf -n 1)'
alias scrot="scrot ~/Pictures/Screenshots/%Y-%m-%d-%H-%M-%S.png -e 'xclip -selection clipboard -t image/png -i ~/Pictures/Screenshots/%Y-%m-%d-%H-%M-%S.png'"
alias mnt="udisksctl mount -b"
alias figlet='figlet -f kmono'
alias cmatrix='cmatrix -bC magenta'
alias stmpd="systemctl start --user mpd"
alias stpa="systemctl start --user pulseaudio"
alias vpnepfl='sudo openconnect --useragent=AnyConnect -b vpn.epfl.ch'
alias vpnjhu='sudo openconnect --protocol=nc -b vpn.jh.edu/linux'
alias resist='~/.custom/resist/./resist'
alias nix-shell="nix-shell --command zsh"
alias nix-dev="nix develop --command zsh"

alias fiji="~/Desktop/Fiji.app/./ImageJ-linux64"

alias ugeneui="~/Desktop/ugene-50.0/./ugeneui"

alias todo='nvim -c "set foldlevel=2" ~/Documents/TODO.md'

alias nmgui="nm-applet    2>&1 > /dev/null &
    stalonetray  2>&1 > /dev/null
    killall nm-applet"

function venv-activate() {
    input_dir="$1"

    if [ -d "$input_dir" ]; then
        VIRTUAL_ENV_DISABLE_PROMPT=1
        source $input_dir"/bin/activate"
    else
        VENV_PATH=~/.myvenvs/
        ret=$(ls $VENV_PATH | bemenu -i)
        if [[ "$ret" == "" ]]; then
            return
        fi
        VIRTUAL_ENV_DISABLE_PROMPT=1
        source $VENV_PATH$ret"/bin/activate"
    fi
    # PS1="\[\033[01;35m\]┌─── \[\033[01;36m\]\w \[\033[0;33m\](${VIRTUAL_ENV##*/})\n\[\033[01;35m\]└ \[\033[00m\]"
}

function mamba-activate() {
    stmamba
    VENV_PATH=~/.micromamba/envs/
    ret=$(ls $VENV_PATH | bemenu -i)
    if [[ "$ret" == "" ]]; then
        return
    fi
    micromamba activate $ret
}

function clock() {
    # str=$(curl -s wttr.in/?format="%l:+%C+%t+%p\n")
    tput civis
    watch -ct -n 1 "echo -n '\033[36;1m';
    date '+%T'|figlet -ct -f kmono;
    echo -n '\033[37;0m';
    date '+%A%t%d/%m/%Y'|figlet -ct -f future;
    echo '\033[37;2m';"
    # figlet -ct -f term $str;"
    tput cnorm
}

function stmamba() {
    export MAMBA_EXE='/home/kalankaboom/.local/bin/micromamba'
    export MAMBA_ROOT_PREFIX='/home/kalankaboom/.micromamba'
    __mamba_setup="$("$MAMBA_EXE" shell hook --shell bash --root-prefix "$MAMBA_ROOT_PREFIX" 2>/dev/null)"
    if [ $? -eq 0 ]; then
        eval "$__mamba_setup"
    else
        alias micromamba="$MAMBA_EXE" # Fallback on help from mamba activate
    fi
    unset __mamba_setup
}
