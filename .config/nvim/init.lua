local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
    local lazyrepo = "https://github.com/folke/lazy.nvim.git"
    local out = vim.fn.system({
        "git",
        "clone",
        "--filter=blob:none",
        "--branch=stable",
        lazyrepo,
        lazypath,
    })
    if vim.v.shell_error ~= 0 then
        vim.api.nvim_echo({
            { "Failed to clone lazy.nvim:\n", "ErrorMsg" },
            { out, "WarningMsg" },
            { "\nPress any key to exit..." },
        }, true, {})
        vim.fn.getchar()
        os.exit(1)
    end
end
vim.opt.rtp:prepend(lazypath)

--[[
-- Options
--]]
vim.opt.title = true
vim.opt.titlestring = "%t"
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.cursorline = true
vim.opt.undofile = true
vim.opt.wrap = false

vim.opt.tabstop = 2
vim.opt.shiftwidth = 4
vim.opt.expandtab = true

vim.opt.scrolloff = 3
vim.opt.splitright = true
vim.opt.splitbelow = true

vim.opt.signcolumn = "number"

vim.opt.foldmethod = "expr"
vim.opt.foldexpr = "nvim_treesitter#foldexpr()"

--[[
-- Keymaps
--]]
vim.g.mapleader = " "
vim.g.maplocalleader = "\\"

vim.keymap.set({ "n", "v" }, "<leader>y", [["+y]])
vim.keymap.set("n", "<leader>Y", [["+Y]])
vim.keymap.set("x", "<leader>p", [["_dP]])
vim.keymap.set("n", "<leader>s", [[/<C-r><C-W><CR>N]])

vim.keymap.set("n", "n", "nzz")
vim.keymap.set("n", "N", "Nzz")

vim.keymap.set("n", "<leader>h", "<C-w>h")
vim.keymap.set("n", "<leader>j", "<C-w>j")
vim.keymap.set("n", "<leader>k", "<C-w>k")
vim.keymap.set("n", "<leader>l", "<C-w>l")

vim.keymap.set("i", "<C-c>", "<ESC>")
vim.keymap.set("i", "<C-[>", "<ESC>")

vim.keymap.set("n", "<leader>xx", "<cmd>%!xxd<CR><cmd>set ft=xxd<CR>")

vim.keymap.set("i", "<A-1>", "é")
vim.keymap.set("i", "<A-2>", "è")
vim.keymap.set("i", "<A-3>", "ê")
vim.keymap.set("i", "<A-4>", "à")
vim.keymap.set("i", "<A-5>", "ù")
vim.keymap.set("i", "<A-6>", "û")
vim.keymap.set("i", "<A-7>", "ç")
vim.keymap.set("i", "<A-8>", "ô")
vim.keymap.set("i", "<A-9>", "î")
vim.keymap.set("i", "<A-0>", "ï")

vim.api.nvim_create_user_command("NonAscii", function()
    local success, err = pcall(function() vim.cmd([==[/[^\d0-\d127]]==]) end)
    if not success then print(err) end
end, {})

-- Setup lazy.nvim
require("lazy").setup({
    import = "plugins",
    -- Configure any other settings here. See the documentation for more details.
    -- colorscheme that will be used when installing plugins.
    -- install = { colorscheme = { "habamax" } },
    -- automatically check for plugin updates
    checker = { enabled = false },
})

--[[
-- Autocomp
--]]
local function file_exists(name)
    local file = io.open(name, "r")
    if file ~= nil then
        io.close(file)
        return true
    end
    return false
end

local function start_comp_message()
    vim.api.nvim_echo({
        { "Compilation started!\n", "MoreMsg" },
    }, true, {})
end

local function end_comp_message()
    vim.api.nvim_echo({
        { "Compilation stopped!\n", "WarningMsg" },
    }, true, {})
end

local autocomp = true
function Auto_compile(compile_cmd, cleanup_cmd, view_cmd, view_without_comp)
    if not autocomp then
        vim.api.nvim_clear_autocmds({ group = "autocomp" })
        autocomp = true
        end_comp_message()
        return
    end

    local autogroup = vim.api.nvim_create_augroup("autocomp", { clear = true })

    local jobstart_pre = "call jobstart('"
    local jobstart_post = "', {'detach':1})"

    vim.api.nvim_create_autocmd("BufWritePost", {
        pattern = "*",
        group = autogroup,
        command = jobstart_pre .. compile_cmd .. jobstart_post,
    })

    if cleanup_cmd ~= "" then
        vim.api.nvim_create_autocmd("QuitPre", {
            pattern = "*",
            group = autogroup,
            command = jobstart_pre .. cleanup_cmd .. jobstart_post,
        })
    end

    if view_cmd ~= "" and view_without_comp ~= nil then
        if view_without_comp then
            vim.cmd(jobstart_pre .. view_cmd .. jobstart_post)
        else
            vim.cmd(
                jobstart_pre
                    .. compile_cmd
                    .. " && "
                    .. view_cmd
                    .. jobstart_post
            )
        end
    end
    autocomp = false
    start_comp_message()
end

function Auto_compile_tex()
    local latexmk_cmd = "latexmk -quiet -shell-escape -outdir="
        .. vim.fn.expand("%:p:h")
        .. " -pdfxe "
        .. vim.fn.expand("%:p")

    local path_and_name = vim.fn.expand("%:p:r")

    local tmp_file_ext = {
        ".bcf",
        ".thm",
        ".fls",
        ".out",
        ".aux",
        ".bbl",
        ".blg",
        ".fdb_latexmk",
        ".log",
        ".run.xml",
        ".toc",
        ".xdv",
    }

    for i = 1, #tmp_file_ext do
        tmp_file_ext[i] = path_and_name .. tmp_file_ext[i]
    end

    local cleanup_cmd = "rm " .. table.concat(tmp_file_ext, " ")

    local zathura_cmd = "zathura --fork " .. path_and_name .. ".pdf"

    local already_comped = file_exists(path_and_name .. ".pdf")

    Auto_compile(latexmk_cmd, cleanup_cmd, zathura_cmd, already_comped)
end

function Auto_compile_mark()
    local pandoc_cmd = "pandoc "
        .. vim.fn.expand("%:p")
        .. " $HOME/Templates/fancy-metadata.yaml --highlight-style $HOME/Templates/pantheme.theme --number-sections --columns=1000 -s -o "
        .. vim.fn.expand("%:p:r")
        .. ".pdf"

    local path_and_name = vim.fn.expand("%:p:r")

    local zathura_cmd = "zathura --fork " .. path_and_name .. ".pdf"

    local already_comped = file_exists(path_and_name .. ".pdf")

    Auto_compile(pandoc_cmd, ".err", zathura_cmd, already_comped)
end

vim.api.nvim_create_autocmd("FileType", {
    pattern = "tex",
    command = [[nnoremap \ll <cmd>lua Auto_compile_tex()<CR>]],
})

vim.api.nvim_create_autocmd("FileType", {
    pattern = "markdown",
    command = [[nnoremap \ll <cmd>lua Auto_compile_mark()<CR>]],
})

-- diagnostics
vim.diagnostic.config({
    virtual_text = false,
    severity_sort = true,
    signs = {
        text = {
            [vim.diagnostic.severity.ERROR] = "",
            [vim.diagnostic.severity.WARN] = "",
            [vim.diagnostic.severity.HINT] = "󰌶",
            [vim.diagnostic.severity.INFO] = "",
        },
        linehl = {},
        numhl = {
            [vim.diagnostic.severity.ERROR] = "DiagnosticSignError",
            [vim.diagnostic.severity.WARN] = "DiagnosticSignWarn",
            [vim.diagnostic.severity.HINT] = "DiagnosticSignHint",
            [vim.diagnostic.severity.INFO] = "DiagnosticSignInfo",
        },
    },
    float = {
        border = "single",
        source = "always",
        header = "",
        prefix = "",
    },
})

vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(vim.lsp.handlers.hover, {
    border = "single",
    title = "",
})

vim.lsp.handlers["textDocument/signatureHelp"] =
    vim.lsp.with(vim.lsp.handlers.signature_help, {
        border = "single",
    })
