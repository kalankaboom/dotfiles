return {
    "nvim-lualine/lualine.nvim",
    lazy = false,
    opts = function()
        local lualine_a = {
            {
                "filename",
                file_status = true,
                newfile_status = true,
                symbols = {
                    modified = "[]",
                    readonly = "[󰛤]",
                    unnamed = "[]",
                    newfile = "[]",
                },
            },
        }

        local lualine_b = { { "branch" } }

        return {
            options = {
                icons_enabled = false,
                -- theme = "kalankaboom",
                component_separators = { left = "", right = "" },
                section_separators = { left = "", right = "" },
                always_divide_middle = true,
                globalstatus = true,
                refresh = {
                    statusline = 100,
                    tabline = 10000,
                    winbar = 10000,
                },
            },
            sections = {
                lualine_a = lualine_a,
                lualine_b = lualine_b,
                lualine_c = {
                    {
                        "diagnostics",
                        symbols = {
                            error = " ",
                            warn = " ",
                            info = " ",
                            hint = "󰌶 ",
                        },
                    },
                },
                lualine_x = { "" },
                lualine_y = { "location" },
                lualine_z = { "progress" },
            },
            inactive_sections = {
                lualine_a = lualine_a,
                lualine_b = lualine_b,
                lualine_c = {
                    {
                        "diagnostics",
                        colored = false,
                        symbols = {
                            error = " ",
                            warn = " ",
                            info = " ",
                            hint = "󰌶 ",
                        },
                    },
                },
                lualine_x = {},
                lualine_y = { "location" },
                lualine_z = { "progress" },
            },
        }
    end,
}
