return {
    "rose-pine/neovim",
    lazy = false,
    priority = 1000,
    opts = {
        dim_inactive_windows = false,
        disable_background = true,
        extend_background_behind_borders = true,

        styles = {
            bold = true,
            italic = true,
            transparency = true,
        },

        groups = {
            border = "muted",
            link = "iris",
            panel = "surface",

            error = "love",
            hint = "iris",
            info = "foam",
            note = "pine",
            todo = "rose",
            warn = "gold",

            h1 = "iris",
            h2 = "foam",
            h3 = "rose",
            h4 = "gold",
            h5 = "pine",
            h6 = "foam",
        },
    },
    config = function() vim.cmd([[colorscheme rose-pine]]) end,
}
