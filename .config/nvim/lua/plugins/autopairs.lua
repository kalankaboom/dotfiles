return {
    "windwp/nvim-autopairs",
    event = "InsertEnter",
    config = function()
        local npairs = require("nvim-autopairs")
        local rule = require("nvim-autopairs.rule")

        npairs.setup()

        npairs.add_rules({
            rule("\\(", "\\)", "tex"),
            rule("\\[", "\\]", "tex"),
            rule("\\left|", "\\right|", { "tex", "markdown" }),
            rule("\\left(", "\\right)", { "tex", "markdown" }),
            rule("\\left\\{", "\\right\\}", { "tex", "markdown" }),
            rule("\\left[", "\\right]", { "tex", "markdown" }),
            rule("\\left]", "\\right[", { "tex", "markdown" }),
            rule("$", "$", "markdown"),
        })
        npairs.get_rules("'")[1].not_filetypes = { "tex" }
    end,
}
