return {
    "goolord/alpha-nvim",
    dependencies = { "nvim-tree/nvim-web-devicons" },
    opts = function()
        local startify = require("alpha.themes.startify")

        function to_binary(num, nb_circ)
            local res = ""
            local it = 1
            while num ~= 0 do
                if num % 2 == 0 then
                    res = " " .. res
                else
                    res = " " .. res
                end
                num = math.floor(num / 2) -- WARN: DEPRECATED with Lua 5.3 `num // 2`
                it = it + 1
            end
            for _ = it, nb_circ, 1 do
                res = " " .. res
            end
            return res
        end

        local year = to_binary(os.date("%y"), 7)
        local month = to_binary(os.date("%m"), 4)
        local day = to_binary(os.date("%d"), 5)
        local weekdays = {
            ["Mon"] = 1,
            ["Tue"] = 2,
            ["Wed"] = 3,
            ["Thu"] = 4,
            ["Fri"] = 5,
            ["Sat"] = 6,
            ["Sun"] = 7,
        }
        local wday = weekdays[os.date("%a")]
        local week_bubbles = string.rep("󰽤 ", wday - 1)
            .. " "
            .. string.rep("󰽤 ", 7 - wday)
        local week_line = string.rep("─", 2 * wday) .. "┘"

        startify.section.header.val = {
            "┌─               ─┐",
            "│  " .. year .. " │",
            "│                 │",
            "│        " .. month .. " │",
            "│                 │   " .. week_bubbles,
            "│      " .. day .. " ├─" .. week_line,
            "└─               ─┘",
        }

        startify.section.mru.val = {
            { type = "padding", val = 1 },
            {
                type = "group",
                val = function() return { startify.mru(10, nil, 20) } end,
            },
        }
        -- disable MRU cwd
        startify.section.mru_cwd.val = { { type = "padding", val = 0 } }
        -- disable file_icons
        -- startify.file_icons.highlight = false
        startify.file_icons.highlight = "Keyword"
        --
        startify.section.top_buttons.val = { { type = "padding", val = 0 } }
        startify.section.bottom_buttons.val = {
            startify.button("e", "", ":ene <BAR> startinsert <CR>"),
            startify.button("q", "", ":qa<CR>"),
        }
        -- startify.section.footer.val = {
        --     { type = "text", val = "footer" },
        -- }
        startify.section.footer.val = {
            { type = "padding", val = 3 },
            {
                type = "text",
                val = {
                    "█▀▀▀▀▀█ █ ▀▄█ ▀▄█▄▀▄▀▄█ █▀█▄▀▄█▄█ ▀▄█▄▀▄█▄▀▄█▄█▄█ █▀█ ▀ █▄█ █▄▀▄▀▄▀▄█ ▀ ▀ █▀█",
                    "█ ███ █ ▄██▀▀▀██▄█▀██▄▄▄▀▀▀▄███▄▀█▀▀▄ ▄ ▄██ ▀▀█▄███▀▀ ▄ ▀▄ ▀ ▄ ▀▄▄█▄█▄ ▀█▀▀▀█",
                    "█ ▀▀▀ █ ██▄ ██▄ █▄█▀▄▀▀██▀██▀▀▄ ▄▀▄█▀██   ▀▄▄██▄▄▀█▀█▀▀▀█▄▄▄ ▄ ▄ ▄▀ ▀▄█▀█ ▀ █",
                    "▀▀▀▀▀▀▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀▀▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀▀▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀▀▀▀▀",
                },
                opts = {
                    hl = "EndOfBuffer",
                },
            },
        }

        return startify.config
    end,
}
