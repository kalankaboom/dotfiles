return {
    "luukvbaal/nnn.nvim",
    lazy = false,
    opts = {
        explorer = {
            width = 24,
            side = "topleft",
        },
        picker = {
            style = {
                width = 0.8,
                height = 0.8,
            },
        },
        replace_netrw = "picker",
    },
    keys = {
        {
            "<leader>f",
            function() vim.cmd([[NnnPicker %:p:h]]) end,
        },
    },
}
