return {
    "mhartington/formatter.nvim",
    opts = function()
        local util = require("formatter.util")

        local prettier = function()
            return {
                exe = "prettier",
                args = {
                    "--stdin-filepath",
                    util.escape_path(util.get_current_buffer_file_path()),
                    "--tab-width",
                    4,
                    "--prose-wrap always",
                },
                stdin = true,
            }
        end

        local clangformat = function()
            return {
                exe = "clang-format",
                args = {
                    "-style=file:$HOME/.config/.clang-format",
                    "-assume-filename",
                    util.escape_path(util.get_current_buffer_file_name()),
                },
                stdin = true,
                try_node_modules = true,
            }
        end

        return {
            logging = true,
            log_level = vim.log.levels.WARN,
            filetype = {
                rust = {
                    function()
                        return {
                            exe = "rustfmt",
                            stdin = true,
                        }
                    end,
                },
                cpp = { clangformat },
                c = { clangformat },
                javascript = { prettier },
                typescript = { prettier },
                css = { prettier },
                html = { prettier },
                yaml = { prettier },
                markdown = { prettier },
                json = { prettier },
                nix = {
                    function()
                        return {
                            exe = "nixfmt",
                        }
                    end,
                },
                python = {
                    function()
                        return {
                            exe = "black",
                            args = {
                                -- "-l 70",
                                "-q",
                                "-",
                            },
                            stdin = true,
                        }
                    end,
                    function()
                        return {
                            exe = "docformatter",
                            args = {
                                "-",
                            },
                            stdin = true,
                        }
                    end,
                    function()
                        return {
                            exe = "isort",
                            args = {
                                "-",
                            },
                            stdin = true,
                        }
                    end,
                },
                lua = {
                    function()
                        return {
                            exe = "stylua",
                            args = {
                                "-f",
                                "~/.config/stylua/stylua.toml",
                                "--stdin-filepath",
                                util.escape_path(
                                    util.get_current_buffer_file_path()
                                ),
                                "--",
                                "-",
                            },
                            stdin = true,
                        }
                    end,
                },
                sh = {
                    function()
                        return {
                            exe = "shfmt",
                            args = { "-i", 4, "-ci" },
                            stdin = true,
                        }
                    end,
                },
                tex = {
                    function()
                        return {
                            exe = "latexindent",
                            args = {
                                "-m",
                                "-g",
                                "/dev/null",
                            },
                            stdin = true,
                        }
                    end,
                },
                ["*"] = {
                    require("formatter.filetypes.any").remove_trailing_whitespace,
                },
            },
        }
    end,
    keys = {
        {
            "<leader>q",
            function()
                vim.cmd([[Format]])
                require("lint").try_lint()
            end,
        },
    },
}
