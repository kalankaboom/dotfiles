local toggled = false
return {
    "xiyaowong/virtcolumn.nvim",
    keys = {
        {
            "<leader>o",
            function()
                if toggled then
                    toggled = false
                    vim.api.nvim_set_option_value("colorcolumn", "", {})
                else
                    toggled = true
                    vim.api.nvim_set_option_value("colorcolumn", "81", {})
                end
            end,
        },
    },
}
