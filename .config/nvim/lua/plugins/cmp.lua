return {
    "hrsh7th/nvim-cmp",
    dependencies = {
        "hrsh7th/cmp-nvim-lsp",
        "hrsh7th/cmp-buffer",
        "hrsh7th/cmp-path",
        "hrsh7th/cmp-cmdline",
        "L3MON4D3/LuaSnip",
        "saadparwaiz1/cmp_luasnip",
        "rafamadriz/friendly-snippets",
    },
    opts = function()
        local icons = {
            Array = "",
            Boolean = "󰨙",
            Class = "󰠱",
            Color = "󰏘",
            Constant = "󰏿",
            Constructor = "",
            Enum = "",
            EnumMember = "",
            Event = "",
            Field = "",
            File = "󰈙",
            Folder = "󰉋",
            Function = "󰊕",
            Interface = "",
            Key = "󰌋",
            Keyword = "",
            Method = "󰊕",
            Module = "",
            Namespace = "󰦮",
            Null = "",
            Number = "󰎠",
            Object = "",
            Operator = "󰆕",
            Package = "",
            Property = "󰜢",
            Reference = "󰈇",
            Snippet = "",
            String = "",
            Struct = "󰆼",
            Text = "",
            TypeParameter = "",
            Unit = "",
            Value = "󰎠",
            Variable = "󰀫",
        }

        local luasnip = require("luasnip")
        require("luasnip.loaders.from_vscode").lazy_load()
        require("luasnip.loaders.from_vscode").lazy_load({
            paths = { "./snips" },
        })

        local cmp = require("cmp")

        cmp.setup.cmdline({ "/", "?" }, {
            mapping = cmp.mapping.preset.cmdline(),
            sources = {
                { name = "buffer" },
            },
        })

        cmp.setup.cmdline(":", {
            mapping = cmp.mapping.preset.cmdline(),
            sources = cmp.config.sources({
                { name = "path" },
            }, {
                { name = "cmdline" },
            }),
        })

        return {
            snippet = {
                expand = function(args) luasnip.lsp_expand(args.body) end,
            },
            formatting = {
                fields = { "kind", "abbr" },
                format = function(_, vim_item)
                    vim_item.menu = ""
                    vim_item.kind = string.format("%s", icons[vim_item.kind])
                    return vim_item
                end,
            },
            window = {
                completion = cmp.config.window.bordered({
                    border = "single",
                }),
                documentation = cmp.config.window.bordered({
                    border = "single",
                }),
            },
            mapping = cmp.mapping.preset.insert({
                ["<C-e>"] = cmp.mapping.scroll_docs(-4),
                ["<C-y>"] = cmp.mapping.scroll_docs(4),
                ["<A-TAB>"] = cmp.mapping.abort(),
                ["<CR>"] = cmp.mapping(function(fallback)
                    if cmp.visible() then
                        if luasnip.expandable() then
                            luasnip.expand()
                        else
                            cmp.confirm({
                                select = true,
                            })
                        end
                    else
                        fallback()
                    end
                end),
                ["<Tab>"] = cmp.mapping(function(fallback)
                    if cmp.visible() then
                        cmp.select_next_item()
                    elseif luasnip.locally_jumpable(1) then
                        luasnip.jump(1)
                    else
                        fallback()
                    end
                end, { "i", "s" }),
                ["<S-Tab>"] = cmp.mapping(function(fallback)
                    if cmp.visible() then
                        cmp.select_prev_item()
                    elseif luasnip.locally_jumpable(-1) then
                        luasnip.jump(-1)
                    else
                        fallback()
                    end
                end, { "i", "s" }),
            }),
            sources = cmp.config.sources({
                { name = "nvim_lsp" },
                { name = "luasnip" },
                { name = "buffer" },
            }),
        }
    end,
}
