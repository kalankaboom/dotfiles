return {
    "mfussenegger/nvim-lint",
    config = function()
        local lint = require("lint")
        lint.linters_by_ft = {
            python = { "pydocstyle", "codespell", "flake8" },
            cpp = { "cpplint" },
        }

        lint.linters.cpplint.args = {
            "--filter=-build/include_order,-legal/copyright,-whitespace/indent,-whitespace/newline,-readability/braces,-runtime/references,-whitespace/parens",
            "--verbose=0",
        }
    end,
}
