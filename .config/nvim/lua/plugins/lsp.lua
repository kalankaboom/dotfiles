return {
    "neovim/nvim-lspconfig",
    dependencies = {
        "williamboman/mason.nvim",
        "williamboman/mason-lspconfig.nvim",
    },
    event = "BufEnter",
    config = function()
        local lspconfig = require("lspconfig")
        local mason = require("mason")
        local mason_lspconfig = require("mason-lspconfig")

        mason.setup({
            ui = {
                icons = {
                    package_installed = "",
                    package_pending = "",
                    package_uninstalled = "",
                },
                border = "single",
            },
        })
        mason_lspconfig.setup()

        local on_attach = function(client, bufnr)
            -- WARN disable Lsp colour changes
            client.server_capabilities.semanticTokensProvider = nil

            local opts = { buffer = bufnr, remap = false }

            vim.keymap.set(
                "n",
                "gd",
                function() vim.lsp.buf.definition() end,
                opts
            )
            vim.keymap.set(
                "n",
                "<CR>",
                function() vim.lsp.buf.hover() end,
                opts
            )
            vim.keymap.set(
                "n",
                "<leader><CR>",
                function() vim.diagnostic.open_float() end,
                opts
            )
            vim.keymap.set(
                "n",
                "<leader>[",
                function() vim.diagnostic.goto_next() end,
                opts
            )
            vim.keymap.set(
                "n",
                "<leader>]",
                function() vim.diagnostic.goto_prev() end,
                opts
            )
            vim.keymap.set(
                "n",
                "<leader>a",
                function() vim.lsp.buf.code_action() end,
                opts
            )
            vim.keymap.set(
                "n",
                "<leader>rr",
                function() vim.lsp.buf.references() end,
                opts
            )
            vim.keymap.set(
                "n",
                "<leader>rn",
                function() vim.lsp.buf.rename() end,
                opts
            )
            vim.keymap.set(
                "i",
                "<A-h>",
                function() vim.lsp.buf.signature_help() end,
                opts
            )
        end

        local capabilities = require("cmp_nvim_lsp").default_capabilities()
        mason_lspconfig.setup_handlers({
            function(server_name)
                lspconfig[server_name].setup({
                    capabilities = capabilities,
                    on_attach = on_attach,
                })
            end,
            ["lua_ls"] = function()
                lspconfig.lua_ls.setup({
                    capabilities = capabilities,
                    on_attach = on_attach,
                    settings = {
                        Lua = {
                            runtime = {
                                version = "Lua 5.4",
                                path = {
                                    "?.lua",
                                    "?/init.lua",
                                    "/usr/local/share/lua/5.1/?.lua",
                                    "/usr/local/share/lua/5.1/?/init.lua",
                                    "/usr/local/share/lua/5.4/?.lua",
                                    "/usr/local/share/lua/5.4/?/init.lua",
                                },
                            },
                            diagnostics = {
                                globals = {
                                    "vim",
                                },
                            },
                            workspace = {
                                library = {
                                    ["/usr/local/share/lua/5.1/"] = true,
                                    ["/usr/local/share/lua/5.4/"] = true,
                                },
                                --     library = vim.api.nvim_get_runtime_file("", true),
                                --     checkThirdParty = false,
                            },
                        },
                    },
                })
            end,
            ["texlab"] = function()
                lspconfig.texlab.setup({
                    capabilities = capabilities,
                    on_attach = on_attach,
                    settings = {
                        texlab = {
                            diagnostics = {
                                ignoredPatterns = {
                                    'siunitx Warning: Detected the "physics"',
                                },
                            },
                        },
                    },
                })
            end,
            ["glsl_analyzer"] = function()
                lspconfig.glsl_analyzer.setup({
                    capabilities = capabilities,
                    on_attach = function()
                        on_attach()
                        vim.keymap.set("n", "<leader>q", vim.lsp.buf.format)
                    end,
                })
            end,
            -- ["clangd"] = function()
            --     lspconfig.clangd.setup({
            --         capabilities = capabilities,
            --         on_attach = on_attach,
            --     })
            -- end,
            -- ["tsserver"] = function() lspconfig.ts_ls.setup({}) end, -- WARN tsserver depracated -> ts_ls
        })
        lspconfig.clangd.setup({
            cmd = { "clangd-19" },
            capabilities = capabilities,
            on_attach = on_attach,
        })
    end,
}
