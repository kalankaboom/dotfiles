return {
    "mistricky/codesnap.nvim",
    build = "make",
    cmd = "CodeSnap",
    opts = {
        save_path = "~/Pictures/codesnap",
        has_line_number = true,
        bg_theme = "grape",
        -- bg_padding = 0,
        bg_x_padding = 64,
        bg_y_padding = 64,
        watermark = "",
        mac_window_bar = false,
        code_font_family = "BlexMono Nerd Font Mono",
    },
}
