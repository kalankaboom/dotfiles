return {
    "folke/trouble.nvim",
    opts = {
        position = "bottom",
        height = 10,
        width = 50,
        mode = "workspace_diagnostics",
        icons = {
            indent = {
                fold_open = " ",
                fold_closed = " ",
            },
        },
        group = true,
        padding = false,
        indent_lines = true,
        auto_open = false,
        auto_close = false,
        auto_preview = true,
        auto_fold = true,
        auto_jump = { "lsp_definitions" },
        use_diagnostic_signs = true,
    },
    keys = {
        {
            "<leader>d",
            function() vim.cmd([[Trouble diagnostics toggle]]) end,
        },
    },
}
