return {
    "numToStr/Comment.nvim",
    event = "BufEnter",
    opts = {
        mappings = {
            basic = true,
            extra = true,
        },
    },
}
