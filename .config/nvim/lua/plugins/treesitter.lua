return {

    "nvim-treesitter/nvim-treesitter",
    build = ":TSUpdate",
    dependencies = {
        "nvim-treesitter/nvim-treesitter-context",
    },
    event = "BufEnter",
    config = function()
        local treesitter_configs = require("nvim-treesitter.configs")
        local treesitter_context = require("treesitter-context")
        treesitter_configs.setup({
            sync_install = false,
            auto_install = false,
            ignore_install = {},
            ensure_installed = "all",
            highlight = {
                enable = true,
                additional_vim_regex_highlighting = false,
            },
            indent = { enable = true, disable = { "" } },
            modules = {},
        })
        treesitter_context.setup({
            enable = true,
            max_lines = 6,
            min_window_height = 0,
            line_numbers = true,
            multiline_threshold = 3,
            trim_scope = "outer",
            mode = "cursor",
            separator = nil,
            zindex = 20,
            on_attach = nil,
        })
    end,
}
